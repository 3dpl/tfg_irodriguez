## Rocks alias specification
Cmnd_Alias ROCKS = /opt/rocks/bin/rocks
## User add for apache specification
Cmnd_Alias USERADD = /usr/sbin/useradd
## Script for launch job as an user
Cmnd_Alias QSUBUSER = /export/scripts/compile/encoleSGEJob.sh

apache ALL=(%projects) NOPASSWD: USERADD, ROCKS, QSUBUSER
