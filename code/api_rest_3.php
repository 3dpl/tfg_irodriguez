[...]/* Escribimos fichero con los datos del cliente*/
//file_put_contents
$parsedUserData = $usrData["uid"].":".$usrData["gidnumber"]." mailto --> ".$usrData["mail"];
$dataFilePath = $extract_path."/datos.txt";
//Get priority from BD
/* Comprobacion de tipo */
[...] 
//Here adding operation to BD
$codeConn->addCodeOperation($user, $opID, "subido", '', '', $fecha_subida, $extract_path);

//Ask for usr priority
$usrConn = new UsrConnection();
$priority = $usrConn->getUserPriority($usrData["uid"]);
echo $usrData["uid"].":".$usrData["gidnumber"]." prority --> ".$priority."<br/>";

//Recibe el usuario (sistema), nombre_carpeta_proyecto, email_usr, tipo_code (cpu/gpu), tipo_paralelismo (mpi/otro), prioridad, n nodos
$output=[];
switch($tipo) {
	case "mpi":
    echo "Compiling MPI code";
    //Script de MPI
    shell_exec("/export/scripts/compile/compileStdCode.sh "
    .$usrData['uid']
    ." ".$name[0]
    ." ".$usrData['mail']
    ." 'cpu'"
    ." 'mpi'"
    ." ".$priority
    ." ".$n_nodos
    ." &> /dev/null &");
[...]/*Resto de tipos*/
}

//Add to log
$res = Log::addLog($usrData['uid'], "Se ha subido el trabajo $name[0]", 1);
