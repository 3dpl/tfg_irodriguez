#!/bin/bash/
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -q gpu.q
#$ -N cudaTest5
#$ -m be
#$ -pe mpi 25
mpirun -np 25  ./jacobi_cuda_aware_mpi -t 5 5 -d  3686