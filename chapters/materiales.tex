

\chapter{Materiales y Métodos}
\label{cha:metodologia}

En este capítulo se describen los diferentes sistemas, tanto hardware como software, así como la metodología aplicada para comparar diferentes HPCs con el sistema propuesto. Para ello, se han seleccionado dos equipos: Euler, un clúster del Instituto Universitario de Investigación Informática (IUII), y el servidor de cómputo GPU del Departamento de Tecnología Informática y Computación (DTIC) y del 3DPLab, Clarke.

\section{Planificación}

El proyecto en cuestión ha tomado una gran cantidad de tiempo ya que en realidad comenzó a gestarse tres años atrás, desde el primer año se planteó de manera voluntario la realización de este proyecto, el segundo año, como prácticas en empresa realizadas en la EPSA y el DTIC  y el tercer año como proyecto final de grado. 

El proyecto se ha divido en cuatro grandes partes. La primera parte que consistió en una toma de contacto con el equipo de la EPSA y decisión del sistema operativo. La segunda parte estuvo más orientada a la instalación y puesta en marcha del clúster. La tercera fue dedicada en la creación de la API para enviar los trabajos y los \textit{scripts} necesarios para ello. Por último, una cuarta parte dedicada el \textit{benchkmarking} del clúster y su comparación con otros equipos disponibles. Todo el proyecto queda reflejado en el siguiente esquema de plantificación temporal y su representación en un diagrama de Gantt (ver Figuras \ref{fig:esquema_proyecto} y \ref{fig:esquema_gant}).

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/proyect_1.jpg}
    \caption{Esquema de fases del proyecto.}
    \label{fig:esquema_proyecto}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/proyect_gan.jpg}
    \caption{Diagrama de Gantt.}
    \label{fig:esquema_gant}
\end{figure}

\section{Hardware}

Dentro de esta sección se describirán todos los componentes hardware que han sido utilizados durante el desarrollo de este proyecto. En particular se especificaran los componentes hardware de los dos superordenadores que se utilizarán para comparar los resultados. De la misma manera se proporcionará la descripción de los componentes del proyecto en cuestión.

\subsection{Euler}

Euler es un clúster que pertenece al Instituto Universitario de Investigación en Informática de la UA (IUII). Es un clúster adquirido por el grupo de computación de altas prestaciones y paralelismo de la Universidad de Alicante, está cofinanciado por el proyecto construcción y optimización automáticas de bibliotecas paralelas de computación científica-UA, del Ministerio de Ciencia e Innovación. En cuanto al hardware, está conformado por 26 nodos de cálculo, los cuales se dividen en dos grupos: los nodos de cálculo CPU y un nodo de cálculo GPU tal y como se muestra en la Figura \ref{fig:eulernodes}.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.5\linewidth}
    \centering\includegraphics[width=195pt]{images/nodo-calculo-euler.jpg}
    \caption{\label{fig:nodo_calc_euler}}
  \end{subfigure}%
  \begin{subfigure}[b]{0.5\linewidth}
    \centering\includegraphics[width=195pt]{images/nodo-gpu.jpg}
    \caption{\label{fig:nodo_gpu_euler}}
  \end{subfigure}
  \caption{\subref{fig:nodo_calc_euler} Nodo de cálculo CPU de Euler y~\subref{fig:nodo_gpu_euler} Nodo de cálculo GPU.}
  \label{fig:eulernodes}
\end{figure}

\subsubsection{Nodos de Cálculo CPU}

Los nodos de cálculo son servidores HP Proliant SL 390 Generación 7. Equipados con dos procesadores Xeon X5660, los cuales disponen de las siguientes características:

\begin{itemize}[noitemsep]
    \item 6 cores a 2,80 Ghz,  Max Turbo 3,2 Ghz
    \item 12 MB cache Level 2 
    \item 64 bit, SSE 4.2
    \item 32 nm , Max TPD 95 w 
    \item Intel QPI Speed 6,4 GT/s, 2 Links 
    \item Soporte Hyperthreading (desactivado)
\end{itemize}

Además de las características CPU, el servidor presenta estas especificaciones hardware:

\begin{itemize}[noitemsep]
    \item Front Bus 800-1333 MHz  
    \item Memoria 48 GB DDR3-1333
    \item 1 disco SFF SATA 7,2 k, 500GB
    \item Dual port 1GbE NIC
    \item 10GbE SFP+ (via ConnectX2)
    \item QDR IB QSFP (via ConnectX2)
\end{itemize}

Estos nodos están centrados en el cálculo, usando librerías paralelas como MPICH o OpenMPI para poder utilizar el potencial paralelo del clúster. Euler presenta 26 de estos nodos con los que en pleno funcionamiento ofrece un total de 312 núcleos de procesamiento CPU con un total de 1.21 TiB de memoria RAM.

\subsubsection{Nodo de Cálculo GPU}

El nodo de cálculo GPU es un servidor HP Proliant SL 390 Generación 7. Equipado con dos procesadores Xeon X5660 los cuales mantienen las especificaciones comentadas anteriormente al igual que las características hardware también comentadas.

Lo que diferencia este nodo del resto es que dispone de tres tarjetas gráficas, concretamente Tesla M2050 (ver Figura \ref{fig:gpu_euler}) que presentan las siguientes características:

\begin{itemize}[noitemsep]
    \item Arquitectura: Fermi.
    \item Memoria: 3 GB GDDR5 \gls{ECC}.
    \item núcleos: 448 \textit{Thread  Processos}.
    \item Rendimiento en operaciones en coma flotante de doble precisión: 515 GFLOPS.
    \item Rendimiento en operaciones en coma flotante de simple precisión: 1.03 TFLOPS.
    \item Velocidad de la memoria: 1.55 GHz.
    \item Interfaz de memoria: 384-bits.
    \item Ancho de banda de la memoria: 148 GB/s.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=8cm,height=5cm]{images/m2050.png}
    \caption{Tarjeta gráfica Tesla M2050 instalada en Euler}
    \label{fig:gpu_euler}
\end{figure}

Este nodo se utiliza principalmente para el uso del paralelismo que se puede extraer de las GPUs. Al disponer de tres GPUs, este nodo presenta un procesamiento bruto de 1.5 TFLOPS en doble precisión y 3.09 TFLOPS en simple precisión. Con un total de 9 GiB de memoria de vídeo.

\subsubsection{Red de Interconexión}

Euler es un clúster orientado al paralelismo y la red de interconexión es muy importante. En concreto, presenta tres redes de interconexión diferentes:

\begin{itemize}[noitemsep]
    \item Red de administración a 1Gbit.
    \item Red de interconexión a 1Gbit.
    \item Red de interconexión de baja latencia por \gls{INFBAND} 4X QDR.
\end{itemize}

Esta estructura es un esquema clásico de interconexión de nodos en un clúster, figura \ref{fig:red_euler}. La red de administración esta orientada al mantenimiento individual de cada uno de los nodos mientras que la red de interconexión de 1Gbit se dedica al control de las consolas y al acceso a los nodos por parte de los programas. Pero la parte principal del clúster es la red de baja latencia. Esta es la parte más importante dado que son necesarios un gran ancho de banda y una baja latencia para minimizar el tiempo de paso de mensajes dentro de un clúster, ya que este puede ser el factor más limitante en el rendimiento total del sistema. Esta red está creada con un switch de \gls{INFBAND} x4 con el protocolo QDR, lo que proporciona un ancho de banda de 40 Gbps brutos y 32 Gbps efectivos y que permite que cada nodo tenga una conexión parecida a la que tendrían si estuvieran integrados en un mismo hardware. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/esquema-de-red-euler.png}
    \caption{Esquema de interconexión de nodos de Euler.}
    \label{fig:red_euler}
\end{figure}
\subsection{Clarke}

Clarke es un superordenador que pertenece al DTIC y esta alojado en laboratorio de I+D 2 en la segunda planta de la Politécnica III. Este es un ordenador orientado al uso de GPUs en concreto para el entrenamiento de redes neuronales de aprendizaje profundo. 

Sus características hardware son las siguientes:

\begin{itemize}[noitemsep]

\item CPU \newline

En el aspecto de la CPU presenta i7-6800K con estas funcionalidades:

\begin{itemize}[noitemsep]
    \item 6 cores a 3,40 GHz, Max Turbo 3,80 GHz.
    \item 15  MB Smart cache.
    \item 64 bit, SSE 4.2,AVX2.
    \item 14 nm , Max TPD 140 w.
    \item Soporte Hyperthreading.
\end{itemize}

Además de las características CPU, Clarke presenta estas especificaciones hardware:

\begin{itemize}[noitemsep]
    \item Front Bus 1333-2133.
    \item Memoria 16 GB DDR4-2666.
    \item 2 discos SATA 7,2 k, 3TB (\gls{RAID1}).
    \item 1 disco SATA SSD, 500GB.
\end{itemize}
 
Pero las partes hardware más importantes son las tres tarjetas gráficas que dispone. Dos de ellas son las que se utilizan para las operaciones de inteligencia artificial y operaciones de cálculo y la tercera está dedicada para salida de vídeo para tareas de mantenimiento. Las especificaciones son las siguientes:

\item NVIDIA Quadro 2000
\begin{figure}[H]
    \centering
    \includegraphics[width=8cm, height=5cm]{images/cuadro2000.png}
    \caption{Tarjeta NVIDIA Quadro 2000}
    \label{fig:gt_730}
\end{figure}
\begin{itemize}[noitemsep]
    \item Arquitectura: Fermi
    \item Memoria: 1024 MB DDR5
    \item núcleos: 192 \textit{CUDA Cores}
    \item Interfaz de memoria: 128-bits
    \item Ancho de banda de la memoria: 41,6 GB/s
\end{itemize}

\item NVIDIA Tesla K40c
\begin{figure}[H]
    \centering
    \includegraphics[width=8cm, height=8cm]{images/tesla_k40c.jpg}
    \caption{Tarjeta Tesla K40c}
    \label{fig:teslaK40c}
\end{figure}
\begin{itemize}[noitemsep]
    \item Arquitectura: Kepler
    \item Memoria: 12 GB GDDR5
    \item núcleos: 2880 \textit{CUDA Cores}
    \item Interfaz de memoria: 384-bits
    \item Ancho de banda de la memoria: 288 GB/s
\end{itemize}
\end{itemize}

La primera que se muestra en el listado es la tarjeta orientada a salida de vídeo para operaciones de depuración del ordenador o fallos puntuales. Aparte de la tarjeta para visualizar, Clarke dispone de dos K40c, estas tarjetas están orientadas a centros de datos y superordenadores y por ello no disponen de salida gráfica, la potencia de su GPU esta orientada únicamente a cálculos paralelos. Esta tarjetas presentan una potencia de 4.29 TFLOPS. Como se ha comentado este es un superordenador orientado a la explotación del paralelismo dentro de la propia tarjeta, más que el uso del paralelismo de CPU como el ejemplo anterior de Euler.

\subsection{Ordis}

Ordis hace referencia al clúster creado en el laboratorio L14 en el edificio EPS 1 perteneciente a la EPSA. Ordis está conformado por 31 nodos de cálculo con características idénticas y un nodo maestro que opera en un entorno virtual localizado en la sala de servidores de ese mismo edificio. Este clúster está orientado a proporcionar un entorno de pruebas para alumnos y profesores, para la programación  en paralelo, así como para aprovechar los recursos hardware de ese laboratorio cuando no se esté utilizando para fines docentes. Uno de los objetivos principales es que los equipos disponibles en el L14 puedan ser aprovechados tanto como un uso normal de clase como un sistema de altas prestaciones.

\subsubsection{Master Node}

Ordis mantiene un esquema clásico de clúster, donde se encuentra un nodo maestro encargado de distribuir las tareas sobre los diferentes nodos de cálculo. Este nodo maestro en nuestro caso corre sobre un entorno virtual. Esto es así para facilitar que  pueda ser mantenido y distribuido entre diferentes máquinas. La máquina virtual presenta estas especificaciones:

\begin{itemize}
    \item Procesador de doble núcleo Genuine Intel KVM a 3Ghz
    \item 2GB de memoria RAM
    \item Disco duro HDD de 100GB
\end{itemize}

El máster no procesa ningún tipo de cálculo solo es encargado de operaciones de coordinación de los nodos y coordinación de trabajos. Por ello las especificaciones hardware no son de extremada importancia en el Master Node.

\subsubsection{Nodos de Cálculo}

Los nodos de cálculo que utiliza el sistema son los ordenadores que hay disponibles dentro del edificio 1 de la EPSA,  en concreto el laboratorio L14. Estos son los ordenadores que se utilizan para las clases normales de diferentes asignaturas que se estudian en la carrera de Ingeniería Informática así como asignaturas de otros grados. Por lo que estos ordenadores no están pensados para ser usados como un clúster híbrido. Pero a pesar de este hecho, presentan unas serie de componentes hardware interesantes para su explotación. En concreto cuentan con  estas especificaciones:

El procesador que utiliza es el Pentium G840 que presenta estas características:

\begin{itemize}[noitemsep]
    \item 2 cores a 2,80 GHz.
    \item 3  MB Smart cache.
    \item 64 bit, SSE 4.2.
    \item 32 nm , Max TPD 65 w.
    \item Sin Soporte Hyperthreading.
\end{itemize}

Además de la CPU, los nodos presentas estas características hardware:

\begin{itemize}[noitemsep]
    \item Front Bus 1066-1333.
    \item Memoria 8 GB DDR3-1333.
    \item 2 discos SATA 4,4 k, 500GB.
    \item Tarjeta de red Realtek 1Gb.
\end{itemize}

También cada nodo dispone de una GPU. En concreto las GTX 480 cedidas por NVIDIA  Figura \ref{fig:GTX_480}.  Estas presentan las siguientes características:

\begin{itemize}[noitemsep]
    \item Arquitectura: Fermi.
    \item Memoria: 1,5 GB GDDR5.
    \item núcleos: 480 \textit{CUDA Cores}.
    \item Interfaz de memoria: 384-bits.
    \item Ancho de banda de la memoria: 177.4 GB/s.
\end{itemize}

\subsubsection{Esquema de Red}

El L14 dispone de una red de interconexión Ethernet a 1 Gb/s. Esta red es la única de que dispone el clúster para el paso de mensajes y el intercambio de archivos. Los nodos se dividen en tres bancadas dentro del laboratorio, todos los nodos y el master están conectados a un mismo switch, ver Figura \ref{fig:dist_ordis}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{images/dirst_ordis.png}
    \caption{Distribución de ordis, con un master node y 31 nodos de procesamiento.}
    \label{fig:dist_ordis}
\end{figure}

\section{Software}

En esta sección se va a describir el software que utiliza cada uno de los sistemas que ya se han comentado. En concreto el software que disponen para el lanzamiento de tareas en cada uno de los superordenadores comentados anteriormente así como librerías y funcionalidades propias de cada sistema.

\subsection{Euler}

Euler presenta en sus nodos el sistema operativo conocido como Centos 7, un sistema operativo orientado a entornos empresariales el cual es un \textit{fork} de RedHat y es de uso gratuito. Además, presenta una característica muy interesante para entornos de producción: cada versión dispone de soporte de actualizaciones durante diez años. Para el sistema de coordinación de lanzamiento de tareas, utiliza SGE o \textit{Sun Grid Engine}, el cual es el encargado de gestionar los trabajos, así como las prioridades y las colas de los mismos. Sobre este software se profundizará con más detalle en la parte de software de Ordis (Sección \ref{ORDISSOTWARE}), ya que se utiliza en el proyecto para la gestión de los trabajos. Así mismo, incorpora una serie de software que se lista a continuación:

\begin{itemize}[noitemsep]
    \item Compiladores.
    \begin{itemize}[noitemsep]
        \item Compilador de C/C++ con optimizaciones para procesadores Intel 
        \item Compilador de Fortran con optimizaciones para procesadores Intel
        \item Compilador de C/C++ de GNU
        \item Compilador de Fortran 95 de GNU
        \item NVIDIA Toolkit con compilador nvcc
    \end{itemize}
    \item Librerías paralelas
    \begin{itemize}[noitemsep]
        \item openmpi-gcc para los compiladores GNU
        \item openmpi-intel para los compiladores de Intel
        \item mvapich-gcc  para los compiadores de GNU 
        \item mvapich-intel  para los compiladores de Intel
        \item mvapich2-gcc para los compilaodres GNU
        \item mvapich2-intel para los compiladores Intel 
        \item impi Implementacion de MPI por intel 
        \item Spark 1.3.1 
    \end{itemize}
    \item Librerías científicas
    \begin{itemize}[noitemsep]
        \item \href{https://web.ua.es/es/cluster-iuii/bibliotecas/cientificas/intel-mkl.html}{Intel MKL}
        \item \href{http://www.nvidia.es/object/cuda-parallel-computing-es.html}{CUDA}
        \item \href{http://pointclouds.org/}{PCL}
        \item \href{https://opencv.org/}{OpenCV}
        \item \href{http://arma.sourceforge.net/}{Armadillo}
        \item \href{https://www.mcs.anl.gov/petsc/}{PETSC}
        \item \href{https://www.r-project.org/}{R}
        \item \href{https://es.mathworks.com/products/matlab.html}{Matlab}
        \item \href{http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html}{JDK}
        \item \href{https://www.python.org/}{Python 2.7.5 y 3.5.4}
        \item \href{http://www.ks.uiuc.edu/Research/namd/}{NAMD Molecular Dynamics Software}
        \item \href{https://pages.nist.gov/fds-smv/}{FDS - Fire Dynamics Simulator}
    \end{itemize}
\end{itemize}

Como se puede constatar, se dispone de una cantidad ingente librerías y entornos paralelos, algunos de ellos son incompatibles entre si. Debido a ello, dentro del sistema operativo existe un sistema conocido como \href{https://web.ua.es/es/cluster-iuii/sistema-operativo/entorno-de-usuario-modules.html}{\textit{modules}}, encargado de cargar las diferentes librerías que se necesiten para cada usuario concreto. Así se evitan problemas de incompatibilidades dentro del clúster ya que cada uno controla las librerías que necesita su proyecto.

\subsection{Clarke}

Clarke dispone como sistema operativo Ubuntu 16.04 LTS. Dentro de este sistema, como no dispone de más nodos, no es necesaria la utilización de un controlador de trabajos como SGE comoe en Euler. Dentro de Clarke, para el control de las librerías y los proyectos de los usuarios se emplea un sistema de contenedores virtuales, en concreto utiliza \href{https://www.docker.com/}{\textit{Docker}}. Con este sistema se permite aislar cada trabajo de forma independiente al resto, de manera similar al uso de un sistema de máquinas virtuales pero más flexible y con menos consumo de espacio y recursos. El proceso es el siguiente: un usuario crea un contenedor de docker donde el usuario instala todo el software que necesite, después en esa instancia de docker correrá su aplicación con las librerías que descargue o prepare de forma aislada a otros contenedores.

\subsection{Ordis}\label{ORDISSOTWARE}

Cuando se presentó el proyecto de creación del clúster, una de las decisiones de mayor importancia fue la elección del sistema operativo, dado que el objetivo era la utilización del clúster como un clúster híbrido para  su uso con CPU y GPU, se debía encontrar un SO que soportase las tarjetas gráficas NVIDIA. Tras una búsqueda encontramos el artículo \cite{gpuclsuter} en el foro de NVIDIA que explicaba de forma superficial el montaje de un clúster híbrido. En el artículo utilizaban Rocks. \href{http://www.rocksclusters.org/}{Rocks} es un sitema operativo enfocado a su uso en clústers, en concreto utilizamos la versión 6.2. Rocks utiliza como sistema operativo base Centos 6.6, que como se comentó presenta la característica de que provee actualizaciones durante diez años desde el lanzamiento de la versión. En el Anexo \ref{INSROCKS} se explica con detalle la instalación completa de Rocks para el caso concreto del L14 de la EPSA.

Es importante recalcar en la instalación el software base que instala en el \textit{master node}. Del software que nos permite disponer el instalador  
se seleccionaron los siguientes paquetes:

\begin{itemize}[noitemsep]
    \item Area 51. Incluye dos programas para el control de seguridad del clúster. El primero es Tripwire, un software encargado de detectar cambios dentro del kernel del sistema y dar un aviso al administrador del sistema, y el segundo es chkrootkit que se utiliza para evitar la toma de control del sistema utilizando \gls{ROOTKIT}. 
    \item Base. Incluye el código base del SO necesario para su funcionamiento.
    \item Fingerprint. Incorpora el software fingerprint encargado de leer una lista arbitraria de ficheros binarios y guardar sus dependencias creando un fichero llamado Swirl, este puede servir para ver si una aplicación puede funcionar en otro sistema. Se utiliza principalmente para la instalación de software dentro de los nodos en Rocks.
    \item ganglia. Importa el software ganglia, con el que es posible la monitorización de los nodos del clúster y extraer información sobre carga de los nodos, consumo de red, nodos activos, nodos caídos, etc.
    \item Hpc.Trae una serie de librerías necesarias para los entornos paralelos en concreto MPI en tres de sus versiones, OpenMPI, MPICH, MPICH2. También dentro del paquete se incluye una serie de benchmarks para medir el rendimiento de la red interna del clúster, en concreto Iperf, stream, IOzone.
    \item Java. Instala la maquina \textit{Java virtual machine} para la ejecución de programas Java.
    \item Kernel. Incluye el kernel del SO, básico para el funcionamiento del sistema.
    \item OS. Incorpora el SO, en concreto Centos 6.6.
    \item Python. Trae el interprete de python en su versión 2.7.
    \item SGE. Instala y configura \textit{Sun Grid Engine} en el clúster.
    \item Web-server. Incorpora un servidor web, necesario para el funcionamiento de ganglia.
\end{itemize}

Dentro de todo este conjunto de software, que se incorpora al sistema durante la instalación del \textit{master node}, incidiremos en el funcionamiento y uso de dos de ellos: el paquete de Ganglia y SGE, ya que estos desarrollan un rol muy importante para el desrrollo de este clúster. 

\subsubsection{Ganglia}

Ganglia es un sistema escalable de monitorización de supercomputadores, clústers y redes de computadores. Este software nos permite ver estadísticas, en directo o en diferido, de los diferentes nodos que lo componen. Dentro de las métricas que incorpora se encuentra uso de CPU, memoria RAM, almacenamiento y consumo de red. Algunas compañías que lo utilizan a nivel empresarial son Cray, MIT, NASA y Twitter. 

Ganglia se compone de tres partes fundamentales: El Ganglia Monitor Daemon o gmond, el Ganglia Meta daemon o gmetad y el PHP-web Front o gweb (ver Figura \ref{fig:ganglia_system}). Utiliza un protocolo multicast para el paso de información. Para la representación de los datos utiliza un esquema XML unido con un \gls{XDR} para poder trasmitir la información entre los nodos de forma eficiente y que soporte diferentes tipos de máquinas dentro del mismo área de monitorización. Esa información es transmitida a \gls{RRDtool} que es la encargada del almacenamiento de la información.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{images/ganglia_system.png}
    \caption{Funcionamiento de Ganglia \cite{ganglia}.}
    \label{fig:ganglia_system}
\end{figure}

\begin{itemize}[noitemsep]
    \item GMOND.

Gmond es un daemon multihilo que se ejecuta en cada uno de los nodos del sistema. Es el encargado de comunicarse con el SO para extraer información sobre el estado de la CPU, memoria, disco, etc. A diferencia de otros sistemas de monitorización, gmond no espera ningún tipo de señal para mandar los mensajes, manda la información a través de broadcast a la red cada cierto tiempo. Los datos son recogidos y enviados utilizando el protocolo \gls{XDR} a la red para ser recogidos por otras herramientas. En la Figura \ref{fig:ganglia_system} se aprecia cómo se recolecta la información dentro del clúster y en concreto en la Figura \ref{fig:ganglia_gmond} se puede ver en detalle el funcionamiento de gmond extrayendo los datos para ser enviados.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{images/gmond.png}
    \caption{Funcionamiento de gmond en dos nodos.}
    \label{fig:ganglia_gmond}
\end{figure}

\item GMETAD.
Gmetad esta alojado dentro del \textit{master node}  y es un programa encargado de extraer la información enviada por gmond dentro del clúster y añadirla a \gls{RRDtool} para su almacenamiento y posterior uso. Además tiene la posibilidad de extraer información de otros gmetad para crear una estructura más jerárquica.


\item GWEB.

GWeb esta también alojado en el \textit{master node} y es el encargado de recuperar la información almacenada en \gls{RRDtool} y mostrarla a través de una interfaz web. Esta nos muestra en detalle la información en vivo del sistema que se está monitorizando así como los registros de uso del mismo durante diferentes espacios temporales. 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/ganglia_ordis.png}
    \caption{Ganglia funcionando en Ordis}
    \label{fig:ganglia_ordis}
\end{figure}

Con estas tres partes, ganglia nos permite monitorizar el estado del clúster durante la ejecución de procesos, en tiempo real con un mínimo impacto en la red. Para más información sobre ganglia se puede consultar el siguiente libro \cite{ganglia_goperators}.

\end{itemize}
\subsubsection{Sun Grid Engine}
\label{sge_exp}

Sun Grid Engine o SGE es un gestor de trabajos para entornos de clúster, creado originalmente para los sistemas propietarios SUN pero tras la compra de de Oracle a SUN, el proyecto se declaró \textit{opensource}, por lo que las versiones han sido mantenidas por la comunidad. El funcionamiento del SGE es como el de un director de orquesta, es el encargado de gestionar los trabajos que son lanzados en el clúster. Para ello, todos los trabajos son gestionados en un nodo maestro, donde está en funcionamiento el núcleo de SGE. Desde dicho núcleo, SGE llama a los demonios de ejecución  en cada uno de los nodos (ver Figura \ref{fig:esquema_sge}).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{images/Sge_components.png}
    \caption{Esquema de funcionamiento de SGE.}
    \label{fig:esquema_sge}
\end{figure}

Para la gestión de los múltiples trabajos en el clúster, SGE utiliza una serie de colas configurables, tanto para la urgencia de los trabajos, como para un hardware en concreto. Además, cada usuario dispone de su prioridad en cuanto a que su trabajo entre dentro de los recursos.  SGE lanza los trabajos con el propio usuario a través de los \textit{execution daemons} por lo que evitamos que algún proceso pueda acceder a partes de la memoria que no le pertenecen. Para el uso del SGE, tanto usuarios como administradores tiene que utilizar una serie de comandos para tanto, lanzar, monitorizar, o parar los trabajos lanzados. A continuación describimos los más importantes.

\begin{itemize}[noitemsep]
\item QSUB.

Este comando es el encargado de insertar los trabajos dentro del clúster. Lo común con el lanzamiento de este comando es lanzarlo pasándole por parámetro un \textit{script} que contiene todos los parámetros de lanzamiento así como el programa o los programas a lanzar dentro de ese trabajo. Un ejemplo de un \textit{script} para qsub es el mostrado en el Código \ref{codeqsub}.

\lstinputlisting[language=bash,caption=Script para lazar un trabajo con Qsub.,label=codeqsub]{code/qsub_example.sh}

Como podemos observar, en la línea 1 marcamos el fichero como un \textit{script} en bash y a continuación en las líneas 2 - 8 se especifican los comandos de qsub. Qsub reconocerá todo lo que se encuentre delante la estructura \#\$ como un parámetro propio. Unos de los más importantes son \textit{-pe} , para la selección del entorno paralelo, \textit{-N} para especificar el nombre del trabajo y \textit{-q} para la selección de la cola en la que depositar nuestro trabajo. Para ver en detalle todos los parámetros se pueden consultar en el manual web de \href{http://gridscheduler.sourceforge.net/htmlman/htmlman1/qsub.html}{qsub}. Después de la especificación de los parámetros todo lo que se especifique después de eso sera lo que se ejecutará en el clúster.

\item QSTAT.\\

Este comando es utilizado para monitorizar los trabajos en el clúster así como comprobar el estado de las colas. Por defecto, cuando se lanza sin ningún parámetro muestra todos los trabajos que están dentro de SGE con el usuario que está utilizando durante el lanzamiento del comando. Suele tener este aspecto mostrado en el  Código \ref{codeqstat} al ser lanzado. 

\lstinputlisting[language=bash,caption=lanzamiento de Qstat.,label=codeqstat]{code/qstat_example.sh}

Como se puede observar, nos proporciona mucha información con respecto al trabajo lanzado. Como por ejemplo el ID del trabajo, la prioridad, el nombre que le hemos asignado, el estado y cuándo empezó. Para una lista completa de comandos y estados, recomendamos visitar la página del  \href{https://web.ua.es/es/cluster-iuii/gestor-de-recursos/qstat-monitorizar-trabajos-y-colas.html}{Euler de la IUII}

\item QMON \\

Cuando se desea configurar los aspectos de SGE, existen dos formas de cambiar su comportamiento. Bien directamente accediendo a los ficheros de configuración o bien podemos utilizar qmon que nos muestra una interfaz GUI que nos permite tanto configurar todos los aspectos de SGE como de ver el estado del clúster así como los trabajos, ejecutados, en ejecución o en espera tal y como se muestra en la Figura \ref{fig:qmon_sge}.

\begin{figure}[H]
    \centering
    \includegraphics[width=13cm]{images/sge.png}
     \centering
    \includegraphics[width=13cm]{images/qcontrol.jpg}
    \caption{Visualización de Qmon}
    \label{fig:qmon_sge}
\end{figure}
\end{itemize}
