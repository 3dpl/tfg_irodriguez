\chapter{Sistema: Front End}
\label{cha:frontend}

En este capítulo describiremos la parte del sistema propuesto dedicada a los usuarios, es decir, el \textit{front end}. En primer lugar, expondremos las necesidades de esta parte de la propuesta en la Sección \ref{sec:frontendintro}. Seguidamente, describiremos las particularidades de la interfaz web en la Sección \ref{sec:interfazweb}. De la misma manera, detallaremos la API REST que orquestra todo el proceso en la Sección \ref{sec:apirest}. Por último, trataremos una serie de \textit{scripts} útiles para esta parte del sistema en la Sección \ref{scripts_api}.

\section{Introducción}
\label{sec:frontendintro}

Como hemos mencionado durante el transcurso de este documento, la configuración, puesta en marcha y uso de un clúster es un proceso bastante complejo. Generalmente, la configuración y puesta en marcha solo es necesario que se realice una vez en el vida útil de clúster. En cambio, dado que son los usuarios quienes más lo van a utilizar, una parte fundamental en el desarrollo de este proyecto fue la creación y puesta en marcha de una interfaz sencilla para que los mismos puedan subir y lanzar proyectos al clúster.

Para implementar esta solución se requieren una serie de condiciones. En primer lugar, es requisito fundamental que pueda integrarse con los sistemas existentes en la EPSA. Segundo, los usuarios no tienen acceso al clúster de forma directa en ningún momento. Tercero, tiene que ser accesible siempre, aunque el clúster no este operativo. Cuarto, tiene que ser fácil de ampliar. Y quinto y último, tiene que ser sencillo de utilizar.

Teniendo en cuenta los requerimientos que se nos propusieron, se decidió el uso de una aplicación web con \textit{API REST}. Esta aplicación \textit{FULL REST} recibe las peticiones desde un servidor de la EPSA hasta el nodo \textit{master} y la aplicación ejecuta una serie de \textit{scripts} para recibir, preparar, lanzar y recoger los trabajos que se van a procesar en el clúster. Esta aplicación seguirá este esquema básico que se muestra en la Figura \ref{fig:scheme_basic}, el cual desarrollaremos en las siguientes secciones.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/funcinamiento_basic.png}
    \caption{Esquema general de la aplicación.}
    \label{fig:scheme_basic}
\end{figure}

\section{Interfaz Web}
\label{sec:interfazweb}

Para comenzar con esta parte del proyecto, describiremos el proceso que sigue un usuario típico para lanzar un trabajo con nuestra aplicación propuesta. En primer lugar, el usuario entra en la interfaz web de la aplicación. Antes de entrar se le preguntan sus credenciales de la EPSA. Con ello, la aplicación garantiza que conocemos el usuario y que está identificado dentro de la EPSA, además extraemos del registro LDAP información referente al usuario, como por ejemplo su email, que será el recipiente al que se le enviarán los correos de la aplicación.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{images/interfaz_web.jpg}
    \caption{Interfaz web de Ordis cortesía de los técnicos de la EPSA.}
    \label{fig:web_interface}
\end{figure}

Desde esta interfaz se nos indica que subamos un fichero comprimido en formato ZIP (tal y como se muestra en la Figura \ref{fig:web_interface}). Este archivo contendrá nuestro trabajo que se va a lanzar en el clúster. Para ser considerado válido para la aplicación, el fichero debe contener una estructura que describimos a continuación.

Tal y como se indica en la Figura \ref{fig:file_api_scheme}, tiene que contener las carpetas include, lib y src, junto al archivo makefile que compilará el proyecto y un archivo opcional param.txt con los parámetros de lanzamiento de la aplicación. Si este archivo contiene más de una línea, se generará un trabajo diferente por cada línea del archivo.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{images/files.png}
    \caption{Esquema de la guía de estilo del fichero ZIP.}
    \label{fig:file_api_scheme}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/esquema_api.png}
    \caption{Diagrama de flujo de la aplicación web}
    \label{fig:web_diagram}
\end{figure}

Tras la subida del fichero se nos muestra un desplegable con las diferentes opciones de posibles trabajos que se pueden lanzar dentro del clúster. Aquí se incluyen las opciones de lanzamiento con MPI, CUDA, MPI + CUDA, Otro paralelismo y No parallel. Cada una de estas opciones genera una configuración diferente (explicado de forma más detallada en la Sección \ref{scripts_api}). Por último, la interfaz nos presenta el campo número de nodos, donde el usuario selecciona el número de nodos que se van a reservar para su trabajo. Este número no puede ser superior a 31 en CUDA + MPI ni a 62 con MPI. Esta barrera se debe a la limitación de nodos de clúster.  Una vez todos los parámetros del trabajo son seleccionados por el usuario, ya puede proceder a pulsar el  botón \textit{upload}. Con ello la web llamará a un método \textit{POST} de la \textit{API REST} que reside en Ordis. Si cumple todos los requisitos comentados anteriormente, el trabajo se habrá subido correctamente al clúster, con lo que la aplicación utilizará una serie de \textit{scripts} para comunicarse con el sistema operativo y con \gls{SGE} para que el trabajo sea lanzado. La Figura \ref{fig:web_diagram} muestra un diagrama de flujo explicativo del proceso anterior.


\section{API REST}
\label{sec:apirest}

Una vez los datos han sido recopilados por la interfaz web, estos son enviados a la \textit{API REST} alojada en Ordis. Su función es la de organización de todo el proceso de envío un trabajo al clúster. La API está escrita en lenguaje PHP utilizando el \href{https://github.com/jmathai/epiphany}{framework Epiphany}. Tanto el lenguaje como el framework fue elegido por recomendaciones directas del grupo de técnicos de la EPSA, ya que casi todos sus servicios de \textit{API REST} están creados en este lenguaje.

Es importante destacar que dicha API necesita una serie de dependencias externas para su funcionamiento, las cuales se muestran en la Figura \ref{fig:api_dependences}. Estas dependencias son necesarias para que la API pueda gestionar correctamente los trabajos así como mantener a los usuarios y administradores conscientes de todo lo que ocurre en el clúster.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/dependias_api.png}
    \caption{Dependencias de API REST.}
    \label{fig:api_dependences}
\end{figure}


A continuación expondremos los diferentes métodos que acepta la \textit{API REST} así como su funcionamiento interno.

El primer método a tratar es aquél que más va ser utilizado durante la vida útil de la aplicación. Este es el método \textit{POST, /operation/code/\$user}. Dicho método es el encargado de subir un nuevo trabajo al clúster para que sea procesado. Como se especifica en el diagrama de la Figura \ref{fig:web_diagram}, el primer paso, después de que un usuario se haya autentificado correctamente en los servidores de la EPSA y haya rellenado el formulario con el trabajo que se desea realizar, es el procesamiento de los parámetros que han llegado a la API. Primero se comprueba de si el usuario existe en el registro de LDAP de EPSA y si además no esta en nuestra lista de usuarios bloqueados por algún mal uso de la aplicación tal y como mostramos en el Código \ref{codeauth}.

\lstinputlisting[language=PHP,caption=Extracción de usuario y verificación de acceso al clúster., label=codeauth]{code/api_rest_1.php}

Después este proceso que comprueba que el usuario existe como alumno de la EPSA y además no está dentro de la lista negra del clúster, se procede a subir el fichero ZIP proporcionado por el usuario y a comprobar que cumple con los criterios obligatorios. Primero se comprueba de que el fichero proporcionado es un fichero ZIP. Los tipos \gls{MIME} aceptados son:

\begin{itemize}[noitemsep]
\item application/zip
\item application/x-zip-compressed
\item multipart/x-zip
\item application/x-compressed
\end{itemize}

Esto nos garantiza que el fichero que contiene el trabajo podrá ser procesado por nuestra API. Después se comprueba que el ZIP cumple con la estructura anteriormente comentada y mostrada en la Figura \ref{fig:file_api_scheme}. A continuación, la aplicación llama a un \textit{script} para comprobar si el usuario ya está creado en el clúster, si no es así lo crea, estos \textit{scripts} serán explicados en profundidad en la Sección \ref{scripts_api}. La API continúa con la comprobación del número de nodos solicitados. Si se supera el número de nodos disponibles por el clúster, la API devuelve un error de número de nodos. Después se produce la comprobación del tipo \gls{MIME} y la descompresión del ZIP proporcionado en el fichero  \textit{home} del propio usuario en el sistema de ficheros del clúster. Esto está expresado en el Código \ref{codeapirest2} de la API.

\lstinputlisting[language=PHP,caption=Creación del usuario y extracción de los ficheros., label=codeapirest2]{code/api_rest_2.php}

Cuando el trabajo ha pasado ya ha pasado por este proceso, se considera aceptado por el sistema y se procede a la llamada del \textit{script} principal: el encargado de la compilación y lanzamiento del trabajo. Para el funcionamiento correcto de este \textit{script} es necesario que la API recoja una serie de datos del usuario y se lo proporcione al \textit{script}. A partir del momento que la API llama al \textit{script} que toma control absoluto sobre el trabajo y la aplicación registra en el log del sistema que el trabajo se ha subido correctamente y que usuario lo ha proporcionado. A partir de ahí  queda a la espera del siguiente usuario. Esto se representa en el Código \ref{codeapirest3} de la API de esta forma.

\lstinputlisting[language=PHP,caption=Recolección de datos de usuario y lanzamiento de la compilación.,label=codeapirest3]{code/api_rest_3.php}

La siguiente operación que dispone la API es la de actualizar un código ya existente. Este método utiliza una metodología muy similar a la que se utiliza para subir un trabajo al \textit{clúster} con la diferencia de que se comprueba si el código que se indica está en una fase de espera, antes de que esté en ejecución o después de la ejecución errónea. Si es afirmativo la API se encarga de subir el código nuevo proporcionado por usuario. Este Código \ref{codeapirest4} seguirá el mismo proceso que se sigue cuando se sube un trabajo nuevo.

\lstinputlisting[language=PHP,caption=Comprobación del estado de un trabajo para su actualización.,label=codeapirest4]{code/api_rest_4.php}

Como última operación que puede realizar la API, existe la posibilidad de que un usuario borre un trabajo. Para ello, como ocurre con la operación de actualización de un trabajo, el trabajo tiene que estar en fase de espera. Este \textit{script} el trabajo de la cola y borrar todos los archivos contenidos en la carpeta del usuario referentes al trabajo en cuestión. El código completo de esta parte de la API se encuentra en el Anexo \ref{CodigoAPI}.

\section{Scripts API}
\label{scripts_api}

Para que la aplicación de los trabajos funcione correctamente fue necesario la creación de varios \textit{scripts}: un \textit{script} para la compilación y lanzamiento de los trabajos, así como uno para la monitorización de la finalización de los procesos y para envío del resultado del trabajo al usuario que lo mandó. Estos \textit{scripts} son necesarios para poder unir la interfaz web con el esquema clásico de trabajo en un clúster con SGE.

\subsection{Script de Compilación}

Cuando la API ya ha cumplido su parte de recibir los datos del usuario y extraer todos los datos necesarios, corre por parte del \textit{script} de compilación la responsabilidad de la generación del ejecutable final y el lanzamiento del trabajo del usuario en el clúster. 

El proceso de compilación sigue varias fases marcadas. La primera es comprobar que la llamada con la que se invoca al \textit{script} desde la API tiene todos los parámetros necesarios. Además, comprueba que el usuario y la carpeta existen en el sistema. Una vez que se ha comprobado que el proyecto tiene todos los archivos que necesita se procede a la fase de compilación del mismo. Utilizando el Makefile, que es obligatorio para el proyecto, se procede a la compilación. Dado que múltiples usuarios pueden lanzar un proyecto al clúster, y que la compilación se ejecuta en el nodo \textit{master} el cual dispone de recursos limitados, se optó por la creación de un sistema basado en semáforos para controlar esta situación.

El sistema funciona de la siguiente manera: una vez el proyecto ha pasado por la comprobación de los ficheros se crea un fichero de texto nombrado como \textit{.lock} que contiene el usuario y el proyecto al que corresponde dicho y se llama a la función de lock que gestionará el semáforo (ver Código \ref{codecompilelock}).\\

\lstinputlisting[language=BASH,firstline=147, lastline=155,caption=Comprobación del Makefile y creación del \textit{.lock}.,label=codecompilelock]{code/compilestdcode.sh}

Esta función lock lo primero que intenta es copiar el fichero en la carpeta \textit{home} de todos los usuarios, utilizando el comando cp con el parámetro -n para evitar que pueda sobrescribir el archivo. Una vez que intenta eso, comprueba si la copia del fichero ha sido satisfactoria, comprobando que el contenido del fichero en \textit{home} forma parte del proyecto que está ejecutando el \textit{script}. Si lo ha conseguido, sale del bucle y empieza la compilación del proyecto. Por el contrario, si no ha sido posible la escritura del fichero, el \textit{script} entra en un \textit{leep} donde cada dos minutos comprueba si ese archivo \textit{.lock} existe, si otro proceso lo borra tras terminar la compilación, sale del bucle y se repite el proceso de sobreescritura del fichero y comprobación de si ha sido posible la reserva. Este proceso se encuentra expresado en el Código \ref{codecompilelocksemaphor}.\\

\lstinputlisting[language=BASH, firstline=24, lastline=45,caption=Sistema de semáforo para la compilación.,label=codecompilelocksemaphor]{code/compilestdcode.sh}

Después de pasar por el semáforo, el flujo se encamina hacia la compilación. Este proceso es lineal por lo que sigue una serie de pasos preestablecidos. Primero se cargan las librerías del sistema, tales como OpenMPI y CUDA, así como el compilador nvcc de CUDA. Después se lanza el comando \textit{make} para empezar el proceso de compilación redirigiendo la salida estándar y la salida de error a un fichero de resultado. Una vez hecho eso, se invoca a un \textit{wait} del sistema de Linux hasta que el proceso de compilación concluya. Cuando este proceso ha concluido, se comprueba si en el fichero resultado hay algún mensaje de error, si se encuentra se elimina el \textit{.lock} y se envía un correo al usuario avisando del error, dando a su vez por finalizado el \textit{script}. Si por el contrario no hay ningún error, se busca en la carpeta del \textit{Makefile} el fichero ejecutable generado y se almacena en una variable, ya que será necesario más tarde para la creación del \textit{run.sh}. A continuación se eliminan los ficheros resultado y todos los ficheros .o generados. Este proceso se muestra en el Código \ref{codecompilation}.\\

\lstinputlisting[language=BASH, firstline=156, lastline=178,caption=Compilación del proyecto.,label=codecompilation]{code/compilestdcode.sh}

Una vez se dispone del proyecto compilado y listo para su ejecución se procede a crear el archivo \textit{run.sh} que será el que finalmente SGE ejecutará. Antes de la creación de dicho archivo se comprueba si existe el archivo \textit{param.txt}, el cual es el encargado de contener los parámetros de lanzamiento de la aplicación compilada. Si dispone de una línea de parámetros se creará el fichero \textit{run.sh} con los parámetros proporcionados. Si por el contrario el archivo contiene más de una línea de parámetros, se procederá a crear un fichero \textit{run.sh} por cada línea que contenga \textit{param.txt}. Este proceso está codificado en el Código \ref{codecompilerunsh}.\\

\lstinputlisting[language=BASH, firstline=180, lastline=198,caption=Comprobación de param.txt para creación de run.sh.,label=codecompilerunsh]{code/compilestdcode.sh}


Tanto tengan parámetros o no se llama a la función createRunfile. Esta función recopila todos los datos proporcionados por parámetro además del nombre del fichero a ejecutar para crear el archivo \textit{run.sh}. En la parte superior del fichero se indican los parámetros de configuración de \gls{SGE} (los cuales dependen de los datos proporcionados al \textit{script}). Adicionalmente, se añade el ejecutable del proyecto con los parámetros si son necesarios. Además, se añaden al \textit{run.sh} una serie de comandos para almacenar en ficheros predefinidos la fecha y hora de inicio, finalización del proyecto así como la fecha y hora a la que acaba el proyecto en milisegundos. Estos ficheros serán necesarios para otros \textit{scripts} que se encargarán de informar al usuario cuando su proyecto ha comenzado, para cuando el proyecto haya finalizado o ya este disponible para descargar, después de que se comprima y se migre al almacenamiento final del proyecto (ver Código \ref{codecompilemore}).\\

\lstinputlisting[language=BASH, firstline=48, lastline=101,caption=Creación de run.sh., label=codecompilemore]{code/compilestdcode.sh}

Para finalizar el \textit{script} llama a \textit{encoleSGEJob.sh} con permisos de  sudo para poder encolar el trabajo. En la Sección \ref{script_sge_job} comentaremos este \textit{script} y sus pormenores en detalle. Todo el proceso descrito anteriormente se encuentra esquematizado en la Figura \ref{fig:flow_compile}.

\begin{figure}[H]
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{images/esquema_compilacion.png}
    \caption{Diagrama de flujo del proceso de compilación}
    \label{fig:flow_compile}
\end{figure}

\subsection{Script de Lanzamiento de Trabajo}
\label{script_sge_job}

Cuando el trabajo del usuario está ya compilado y los \textit{run.sh} listos, la última parte consiste en lanzarlo a \gls{SGE} para que sea él quien gestione las colas, la prioridad y la asignación de recursos. Como se comentó con anterioridad en la parte de \gls{SGE} \ref{sge_exp}, es necesario llamar al comando \textit{qsub} para encolar el trabajo. No obstante, para que el proceso pueda acceder a los archivos el trabajo que se alojan en la carpeta del usuario, es necesario que ese comando se lance desde la shell del propio usuario que solicita el proyecto. Dado que la cadena de procesos la inicia la API, el usuario que lanza los \textit{scripts} es el usuario apache. Por defecto este usuario tiene pocos permisos por motivos de seguridad por lo que cualquier permiso concedido tiene que estar controlado. 
Si queremos que el usuario apache pueda lanzar comandos como otro usuario (tal y como se muestra en el Código \ref{codeotrouser}), este tendría que tener permisos de sudo para poder ejecutar un comando similar a este. 

\lstinputlisting[language=BASH,firstline=15,lastline=15,caption=lanzar un comando como otro usuario., label=codeotrouser]{code/encoleSGE.sh}

Esto representaría una gran brecha de seguridad, dado que cualquier atacante con acceso al usuario apache tendría control absoluto del sistema. Siendo como es apache un servicio que recibe llamadas desde el exterior esto no puede aplicarse. Por ello, en el \textit{sudoers file} de CentOS tenemos que añadir el Código \ref{codeapachecentos} para autorizar a apache pueda lanzar ciertos comandos como sudo sin contraseña. 

\lstinputlisting[language=BASH,caption=Autorización a apache de comandos sudo,,label=codeapachecentos]{code/sudoers.sh}

En concreto se le limita a lanzar tres comandos: lanzar comando para añadir usuarios, lanzar comandos de Rocks para sincronización y el comando para encolar un trabajo. Con ello, aunque tengamos un usuario sin contraseña con permisos de sudo, limitamos las acciones que es capaz de realizar. El código completo del \textit{script} para lanzar comando se encuentra en el Anexo \ref{Codigo}.

\subsection{Script de Control del Sistema}\label{watch_dog}

Una vez nuestro proyecto está encolado, es necesaria una forma de detectar cuándo ha comenzado a procesarse y cuándo ha finalizado, tanto para que el sistema sea consciente como para informar del estado al usuario. Para realizar esta función usaremos un \textit{script} llamado watchDog que se ejecuta cada cinco minutos encargado detectar esos cambios en los trabajos. 

Cuando el \textit{cron} del sistema operativo llama a este \textit{script}, recorre todos los ficheros de proyectos de los usuarios extrayendo datos como el usuario, proyecto o el email. Una vez que dispone de estos datos comprueba si en las carpetas de proyecto existe un fichero \textit{start}. Este fichero es uno de los que crea el \textit{run.sh} \textit{script} para poder controlar el estado de ejecución del trabajo. Al encontrarlo, el \textit{script} manda un correo electrónico utilizando el sistema de correos de la EPSA para informar al usuario de que su proyecto ha comenzado. El formato y envío del email corre a cargo de los servidores de correo de la EPSA.

Además de controlar el inicio de un trabajo la parte más importante es la detección de la finalización de un proyecto. Para ello, de la misma forma que al inicio, el \textit{script} busca un archivo llamado \textit{finish} para detectar la finalización del trabajo. Al detectar este fichero el trabajo ha finalizado, por lo que se dispone a comprimir los resultados de la ejecución. Para ello inserta todos los ficheros del usuario en un \textit{TGZ} a excepción de los archivos utilizados para el control de la aplicación (\textit{start, finish, ...}). Además del archivo \textit{finish}, el \textit{script} de ejecución del trabajo crea un último fichero nombrado como \textit{end} el cual contiene la fecha en la que concluyó el trabajo en milisegundos, esto se utiliza para generar el nombre del archivo \textit{TGZ}.

El nombre del \textit{TGZ} se crea utilizando el nombre del usuario y el nombre del proyecto concatenado con la fecha en milisegundos con la que finalizó el proyecto. Con toda esa cadena generamos un \gls{sha256} que será el nombre del fichero.  Esto se representa en el \textit{script} tal y como se muestra en el Código \ref{codesha}.

\lstinputlisting[language=BASH,firstline=54,lastline=60,caption=Función de creación del TGZ.,label=codesha]{code/watchDog.sh}

Una vez creado se copia el fichero en el servidor de archivos de la EPSA y se informa al usuario de que su proyecto ha finalizado y está disponible para ser descargado, proporcionándole la URL de descarga del mismo.

La razón de la creación del nombre del \textit{TGZ} con esa codificación tan peculiar no es otra que por la necesidad de poder crear un nombre único para el fichero y que sea difícil poder conocerlo. Esto es necesario ya que el servidor de archivos de la EPSA no comprueba qué usuarios pueden descargar qué ficheros, por ello la generación de ese nombre nos garantiza que solo el usuario que reciba el correo es capaz de descargar el archivo.  El código completo del \textit{script} así como de su archivo de configuración se encuentran en el Anexo \ref{Codigo}.

\subsection{Script de Limpieza de Trabajos}\label{cleaner_script}

Además de comprobar si un trabajo ha comenzado o concluido, es importante la gestión del espacio. En nuestro clúster la capacidad de almacenamiento de trabajos es baja. Al ser un sistema abierto a un público relativamente general se espera un tránsito de ficheros considerable. Es por ello que fue necesario implementar un \textit{script} para la gestión de la eliminación de los trabajos, tanto la carpeta de proyectos como los comprimidos. El \textit{script} realiza dos pasadas: la primera por todas las carpetas de los proyectos y la segunda por la carpeta montada con los proyectos en \textit{TGZ}. El \textit{script} realiza la misma acción en cada una de ellas. Primero localiza los ficheros y extrae la fecha de modificación de los mismos. A continuación la compara con el parámetro del tiempo máximo y si lo supera borra el proyecto en cuestión. Este comportamiento queda reflejado en el Código \ref{cod:erase}.E l código completo se encuentra disponible en el Anexo \ref{cod:delete.sh}.

\lstinputlisting[language=BASH,firstline=8,lastline=22,caption=Proceso de borrado de proyectos antiguos , label=cod:erase]{code/deleteProyects.sh}
