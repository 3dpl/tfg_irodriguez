\chapter{Sistema: Back End}
\label{cha:backend}

En este capítulo se describen los cambios realizados sobre el sistema operativo base Rocks 6.2 para que fuera operativo en el laboratorio L14 que y que fuese posible integrarlo con los sistemas de la EPSA.

Como ya se comentó en la Sección \ref{ORDISSOTWARE}, el sistema operativo elegido fue Rocks 6.2 dada su orientación a la creación de clústers. Aunque \textit{off the shelf} Rocks tiene la capacidad de crear clústers con solo las configuraciones mínimas del mismo, estas no eran válidas por el entorno en el que iba a ser desplegado. Nuestro caso es  particular, necesitamos que el sistema sea capaz de funcionar tanto como una clase normal los días lectivos, y como un clúster los días no lectivos. Por ello, era necesario cambiar aspectos de funcionamiento, desde el sistema de instalaciones automáticas, como los sistemas de monitorización y gestión del propio clúster.

A lo largo de este capítulo tratamos por una parte, en la Sección \ref{instalaciones_auto}, el sistema de instalaciones automáticas. Por otro lado, en la Sección \ref{sec:sge} describimos el sistema de gestión y monitorización. Por último, en la Sección \ref{scripts_int}, tratamos los diversos scripts programados para el funcionamiento y mantenimiento del clúster.

\section{Sistema de Instalaciones automáticas} \label{instalaciones_auto}

Como se describe en el Anexo \ref{INSROCKS}, la instalación del nodo maestro no requiere ningún cambio importante, en cambio el resto de nodos necesitan una configuración más específica. Rocks utiliza un sistema de instalación mediante \gls{PXE} y \gls{TFTP} para la instalación de los nodos de cálculo. Por defecto, en el momento en el que instala los nodos, Rocks instala también su propio sistema de arranque  y fuerza a que dicha instalación sea la única. No obstante, para que el sistema funcione con los sistemas operativos que ya existen en el laboratorio L14, es necesario cambiar la forma en la que se instalan los nodos mediante el sistema de \gls{PXE}. 

Antes de ello necesitamos conocer, en detalle, los sistemas operativos presentes en los computadores del L14. En sus ordenadores están instalados los sistemas operativos Windows 10 y Ubuntu 16.04, ambos gestionados por un sistema de arranque \gls{LiLo}. Estos dos sistemas se encuentran alojados en el primer disco duro de $500$ GB que disponen los equipos. Como el sistema que proponemos debe ser capaz de funcionar respetando los sistemas ya creados, utilizamos el segundo disco duro del que disponen los equipos para instalar el sistema operativo y así aislar los ya instalados con el nuestro. Además, es necesario que los nodos de Rocks utilicen \gls{LiLo} en vez del sistema de arranque por defecto, \gls{GNU GRUB}. 

Con esto en mente, procedemos a modificar el XML que utiliza el \gls{PXE} para la instalación. Este XML se encuentra en la ruta \textit{/export/rocks/install/site-profiles/6.2/nodes/} con el nombre \textit{replace-partition.xml}. En él indicaremos los cambios necesarios para que los nodos se instalen con nuestras necesidades específicas. La primera parte es indicar al instalador la ruta en la que instalar el sistema operativo. Para ello procederemos a indicarlo tal y como se muestra en el Código \ref{codepxe}.

\lstinputlisting[language=XML,firstline=15,lastline=17,caption=Especificación del disco de instalación PXE., label=codepxe]{code/rocks_instalatin_nodes.xml}

En estas líneas definimos que no queremos realizar ningún cambio en el disco duro, después indicamos que queremos que la raíz del sistema \textit{/} se instale en el disco 2 partición 4. Por último, definimos que nuestra partición de \gls{SWAP} está en el primer disco partición 3. Con estos cambios permitimos que nuestra instalación sea compatible con las instalaciones ya existentes en el laboratorio L14. 

Dado que utilizaremos el cargador \gls{LiLo}, es necesario que el archivo de imagen del kernel esté en el directorio de archivos de arranque. Para ello, se extrae el archivo en cuestión se extrae de uno de los nodos y se coloca en el sistema de archivos de \gls{LiLo}. La instalación de esta imagen en cada uno de los ordenadores del L14 corre a cargo del grupo de técnicos de la EPSA mediante su propio sistema de instalaciones. 

Aprovechando que utilizamos el archivo para gestionar la localización en la que se instala nuestro sistema operativo, añadimos para la parte final de la instalación una serie de paquetes que serán necesarios para el lanzamiento de pruebas. Estos se dividen en dos grupos principalmente: (1) Los orientados a librerías y compilación como g++, gcc, Fortran, etc.. y (2) los orientados al funcionamiento de las tarjetas gráficas, driver, CUDA, etc.

La instalación de la paquetería de librerías y compilación no representa ningún problema, solo es necesaria la instalación de los propios paquetes y sus dependencias. En cambio, toda la paquetería orientada a las tarjetas gráficas necesita una atención especial. En primer lugar, es necesario instalar una serie de librerías para cubrir las dependencias de CUDA. Después de ello, se debe instalar el driver de las tarjetas gráficas. En este sentido, CentOS instala por defecto el driver \textit{noveau} (la versión libre para los drivers de las tarjetas gráficas NVIDIA). Por ello, es necesario deshabilitarlo para instalar el driver propietario, esto se realiza después de la instalación del sistema tal y como se indica en el Código \ref{cudadriverinstallation}.

\lstinputlisting[language=XML,firstline=90,lastline=95,caption=Instalación del driver de CUDA., label=cudadriverinstallation]{code/rocks_instalatin_nodes.xml}

A continuación se procede a la instalación del driver ya descargado con el parámetro -s  para no requerir entrada del usuario. El único problema es que para la instalación de CUDA es necesario que \textit{noveau} esté desactivado por completo, por ello no se puede realizar durante la instalación y hay que realizarlo más tarde, tal y como se muestra en la Sección \ref{scripts_int}.

\section{SGE}
\label{sec:sge}

El gestor \gls{SGE} se encuentra preinstalado y listo para usar con sus funciones básicas dentro del propio Rocks. No obstante, para nuestro caso es necesario realizar una serie de cambios para poder operar con las tarjetas gráficas.

El primero de todos es la actualización de la librería OpenMPI, ya que en la versión que trae el sistema operativo por defecto no es compatible con CUDA. Para ello se descargó la ultima versión disponible y se procedió a compilarla en el nodo maestro. Una vez todo el proceso de compilación terminó, se procedió a copiar la librería en todos los nodos de cálculo del clúster.

Uno vez hecho esto, deberemos especificar a SGE que nuestro clúster dispone de otro recurso adicional como es la GPU. Esto se realiza modificando el archivo de configuración de valores complejos de \gls{SGE}, en el cual simplemente hay que añadir la variable GPU tal y como se muestra en el Código \ref{sgegpu}.

\begin{lstlisting}[language=Bash,caption=Añadido de la variable GPU a SGE., label=sgegpu]
name   shortcut  type   relop requestable consumable  
gpu     gpu       BOOL    ==      YES          NO
\end{lstlisting}

Dado que vamos a tener trabajos heterogéneos tanto solo CPU como CPU-GPU es necesario especificar a SGE que existen dos tipos de colas de para los mismos: (1) la cola de trabajo que únicamente va a reservar un número de CPUs para ejecución exclusiva en procesadores convencionales y (2) la cola que va a reservar un número de GPUs para ejecución CPU-GPU. El problema radica en que para poder utilizar la GPU, necesitamos reservar una CPU también. Por ello, si aplicamos un esquema clásico de colas dispondríamos de 31 procesadores (también denominados \textit{slots}) para la cola CPU y 31 \textit{slots} de CPU-GPU. Esto hecho generaría cierto desaprovechamiento de recursos ya que por ejemplo un trabajo que necesite 60 \textit{slots} no se podría lanzar aunque no haya ningún trabajo utilizando GPUs. Es por ello que al crear las colas de trabajo de \gls{SGE} se especifico que los trabajos que van a la cola CPU pueden utilizar \textit{slots} de la cola GPU, pero los lanzados en la cola GPU única y exclusivamente pueden utilizar recursos de esa cola. Para ello, se modificó la cola CPU para que tuviera de subordinada a la cola GPU.  De esta forma, en caso de que los \textit{slots} de la cola CPU se llenen, comenzará a utilizar recursos de la cola GPU mientras esté disponible. Los archivos de configuración completos se encuentran en el Anexo \ref{sge_config}.

%¿RCUDA?
\section{Scripts}\label{scripts_int}

En esta sección se describen los diferentes \textit{scripts} utilizados para el funcionamiento y mantenimiento del clúster.

\subsection{Instalación de CUDA}

Para el funcionamiento completo del sistema también fue necesario la implementación de una serie de \textit{scripts} para ejecutar ciertas acciones en el clúster de una forma sencilla. El primero de este repertorio de \textit{scripts} es el encargado de la instalación de CUDA en los nodos del clúster. Esto es necesario ya que como se comentó en el apartado de instalaciones automáticas (ver Sección \ref{instalaciones_auto}) es necesario un reinicio del sistema para poder instalar CUDA. Este \textit{script} consta de dos partes diferenciadas. La primera consiste en la copia de los paquetes de instalación en la carpeta temporal de los nodos de forma iterativa (ver Código \ref{cudacopy}), esto se realiza así para evitar congestionar la red ya que los paquetes de instalación son pesados. La segunda parte consiste en instalar CUDA de forma paralela en los nodos de cálculo tal y como se muestra en el Código \ref{cudainstallation}.\\

\lstinputlisting[language=Bash,firstline=2,lastline=3,caption=Copia del runfile de  CUDA de forma iterativa.,label=cudacopy]{code/installCudaPost.sh}

\lstinputlisting[language=Bash,firstline=6,lastline=7,caption=Instalación de CUDA de forma paralela., label=cudainstallation]{code/installCudaPost.sh}

Además de este \textit{script}, para instalar CUDA en todos los nodos del clúster se implementó una variante del mismo que únicame instala el paquete CUDA en la lista de nodos que se le pase por parámetros. Esto fue necesario ya que por cualquier tipo de problemas, uno o más nodos pueden necesitar ser reinstalados y por ello es conveniente instalar CUDA de forma selectiva solo en ese grupo de nodos. Estos dos \textit{scripts} se encuentran de forma completa en el Anexo \ref{CUDA_nodos}.

\subsection{Inicio y apagado del clúster}\label{start_stop}

Una tarea importante del clúster que era necesario implementar fue la gestión del arranque y parada del mismo. Esto es necesario por el hecho de que nuestro clúster va a tener que iniciarse y apagarse en repetidas ocasiones durante su vida útil. Esto difiere de un clúster tradicional en el cual solo se inicia una vez y se apaga por completo cuando se va a retirar del funcionamiento. Es por ello que se implementaron una serie de \textit{scripts} para gestionar el apagado y el encendido del clúster. La gestión del arranque tiene dos partes: la primera parte es la encargada de gestionar el arranque por \gls{WOL} y la segunda es la responsable de configurar los nodos para aceptar trabajos.

Durante la parte de arranque de los nodos, el \textit{script} de inicio lanza el comando \gls{WOL} a todos los nodos del clúster para comenzar su arranque. Una vez lanzado espera cinco minutos y lanza un comando \textit{dummy} para determinar qué nodos se han despertado. Si algún nodo no ha tenido tiempo de iniciarse o no puede iniciarse por algún motivo, se vuelve a lanzar el \gls{WOL} por segunda vez y el \textit{script} espera de nuevo cinco minutos. Al pasar este tiempo se lanza de nuevo el comando \textit{dummy} para volver a verificar qué nodos están activos. Si sigue habiendo nodos que no se han iniciado, se consideran como nodos caídos del sistema y se apuntan en el log del \textit{script}. Simultáneamente se envía un correo al administrador del clúster para notificar los nodos afectados por la caída para una posible revisión si se repite el fallo de estos nodos a lo largo del tiempo. Esta primera parte de arranque se encuentra reflejada en el Código \ref{cod:start_wake}.\\

\lstinputlisting[language=Bash,firstline=20,lastline=70,caption=Arranque de los nodos del clúster., label={cod:start_wake}]{code/start.sh}

Después de este proceso de inicio de los nodos, se procede a la configuración de los mismos para procesar trabajos. En primer lugar, se exporta la carpeta \textit{home} a todos los nodos del clúster para que dispongan de acceso a los archivos de cada uno de los trabajos. A continuación, se ejecuta un comando en todos los nodos para cambiar las tarjetas a modo \textit{Exclusive Process} con la finalidad de garantizar que solo un proceso puede entrar a la gráfica a la vez. Para finalizar, se desbloquean los procesos de la cola de trabajos. Esto es necesario para evitar que durante el proceso de encendido \gls{SGE} comience a lanzar trabajos cuando el clúster aún no se ha iniciado y configurado por completo. Todo este proceso se expresa en el Código \ref{cod:start_conf}.\\

\lstinputlisting[language=Bash,firstline=71,lastline=77,caption=Configuración de los nodos al arranque., label={cod:start_conf}]{code/start.sh}

Cuando es necesario apagar el clúster, la primera acción a realizar es el bloqueo de los trabajos que todavía no están en ejecución. Este proceso ocurre media hora antes del apagado del clúster (se detalla en la parte de la gestión del \textit{cron} en la Sección \ref{desc_cron}). Después del bloqueo de los trabajos se procede al apagado de los nodos de forma iterativa con el comando \textit{shutdown}, tal y como se muestra en el \textit{script} del Código \ref{cod:stop_conf}.\\

\begin{lstlisting}[language=Bash,caption=Apagado de los nodos., label={cod:stop_conf}]
#!/bin/bash
/opt/rocks/bin/rocks run host compute "shutdown -h now" 
\end{lstlisting}

Todos los \textit{scripts} encargados de la gestión del apagado y encendido del clúster se encuentran en el Anexo \ref{start_stop_script}.

\subsection{Gestión temporal de los \textit{scripts}}\label{desc_cron}

En consecuencia de las características particulares de este clúster, algunos de los \textit{scripts} que se han explicado en este documento deben lanzarse durante períodos de tiempo concretos para el correcto funcionamiento del clúster. Para ello, utilizando la herramienta \textit{cron}, se programaron los diferentes \textit{scripts} que tienen que ser lanzados durante ciertos intervalos. El primero es el encargado de llamar al \textit{script} start.sh cada viernes a las 11:00. El siguiente en la cronología se trata de holdLastWorks.sh  a las 11:30 del domingo. Por último, el apagado con shutdownNodes.sh  a las 11:55 para gestionar el apagado del clúster (ver Anexo \ref{Codigo_nodos} para ver el código completo).

Además de los \textit{scripts} anteriormente comentados, que se encargan del arranque y parada de los nodos, dentro del \textit{cron} se especifican dos \textit{scripts} que se ejecutan de forma periódica todos los días. El primero es el encargado del control de los proyectos, para determinar cuándo comienzan y cuándo finalizan (ver Sección \ref{watch_dog}). Dicho \textit{script} es lanzado cada cinco minutos por el \textit{cron}. El último en la lista del \textit{cron} es el encargado de la limpieza de los trabajos dentro del clúster (ver Sección \ref{cleaner_script}). En este caso, se lanza todos los días a la 1:00 para limpiar todos los trabajos con más de dos semanas de antigüedad. La configuración completa del \textit{cron} está descrita en el Anexo \ref{cod:cron.sh}.

%rcuda scripts ¿?\\