\chapter{Introducción}
\label{cha:introduccion}

En este capítulo se realizará un breve repaso histórico a la computación y los avances en los procesadores (Sección \ref{sec:historia}). Después se revisan una serie de trabajos relacionados con el campo de la supercomputación. Por su relación con este proyecto, se observan diferentes aspectos de la supercomputación (Sección \ref{sec:motivacion}), así como trabajos relacionados con el diseño y empleo de clústers \gls{CPU}, \gls{GPU} y \gls{CPU}/\gls{GPU} (Sección \ref{sec:trabajos}).
Para finalizar se plantea la propuesta del trabajo (Sección \ref{sec:propuesta}) y se define la estructura del documento (Sección \ref{sec:estructura}).

\section{Historia de la computación}
\label{sec:historia}

Con el comienzo de la computación moderna, el reto de crear un computador cada vez más rápido se extendió por la comunidad tecnológica de la época en la década de los 60s. Así, tal fue el impacto, que Gordon Earl Moore proponía en su publicación más relevante, la famosa ley de Moore \cite{moore1998cramming}, que establece que el número de transistores por unidad de superficie en un procesador se duplica cada año. Con este postulado, Gordon E. Moore junto a Robert Norton Noyce, crearon la empresa Intel. Sobre el año 1970, a esta empresa poco conocida, se le propuso realizar el primer microprocesador de la historia y el 15 de diciembre de 1971 lo lanzaron al mercado, figura  \ref{fig:Intel_4004}. Este microprocesador sentó las bases sobre las que se aplicaría la ley de Moore.

\begin{figure}[H]
    \centering
    \includegraphics[width=4cm]{images/Intel_4004.jpg}
    \caption{Microprocesador Intel 4004}
    \label{fig:Intel_4004}
\end{figure}
En 1975, Moore actualizó su ley rebajando las expectativas de duplicación del número de transistores por unidad de superficie a 24 meses. Esta modificación se aplicó debido a las dificultades surgidas en la reducción de tamaño de los transistores, que quedaron reflejadas en la salida del procesador de Intel 8080. Y se ha estado cumpliendo desde entonces hasta 2004 con la salida del procesador Pentium 4.

Por otra parte, en el año 2000 una pequeña empresa conocida como Nvidia lanzó al mercado una tarjeta gráfica que disponía de una Unidad de Procesamiento Gráfico (más conocida como GPU en inglés), figura \ref{fig:GF_256}. Dicho avance dio pie a que el procesador pudiera centrarse en las tareas del cálculo más puro, relegando así a este segundo “procesador” las tareas de mostrar gráficos por pantalla. Al disponer de un hardware específico para el procesamiento de gráficos, las interfaces de usuario UI, comenzaron a hacerse más y más ricas en contenido exigiendo cada vez más rendimiento a estas nuevas GPU's.

\begin{figure}[H]
    \centering
    \includegraphics[width=7cm]{images/geforce_256.jpg}
    \caption{GeForce 256 Primera GPU }
    \label{fig:GF_256}
\end{figure}

Sobre los años 2001 a 2003 se observó que se podía aprovechar la potencia de estos nuevos procesadores gráficos en tareas de cálculo en coma flotante y cálculo de sistemas de matrices con gran eficiencia. En 2001 la comunidad científica  consiguió utilizar algunas de las GPU para el cálculo de multiplicación de matrices, una tarea paralela que los procesadores de la época tenían dificultades en procesar. En 2005 se consiguió implementar la factorización LU aplicando esta metodología. Con estos pioneros se empezó a acuñar el término \textit{General-purpose computing on graphics processing units} o GPGPU, el cual se refería a la utilización de las GPU como un procesador de propósito general para tareas paralelas, y  no solo para procesamiento de gráficos. 

Sin embargo, no se disponía de una API de alto nivel para poder realizar estas tareas, por lo que para ejecutar estos primeros algoritmos paralelos se tenia que \textquotedblleft engañar\textquotedblright \hspace{0.5mm}  a las tarjetas gráficas pasando los datos como una imagen y utilizando los \textit{shaders} para pasar las operaciones que tenían que realizarse en cada celda de datos. Esto provocaba que solo el personal que tuviera alto conocimiento de la arquitectura y de su programación a bajo nivel, fuera capaz de desarrollar software. 

En 2004, con la salida del procesador Intel Pentium 4, se constató que no se podía seguir aumentando la velocidad de reloj del procesador. Esto se debía a que el aumento de la frecuencia de reloj provocaba que la temperatura de los microprocesadores aumentara de forma exponencial. Por lo que, en 2005 Intel presentó los primeros procesadores comerciales multinúcleo, dando paso así a los sistemas de procesamiento paralelos en la CPU.

En 2007, Nvidia puso a disposición de la comunidad CUDA, acrónimo de \textit{Compute Unified Device Architecture}. Este hecho, supuso un hito que popularizó el uso de las GPU como procesadores de propósito general (GPGPU), permitiendo que estas tarjetas gráficas pudieran ser programadas utilizando el lenguaje C++. Al ser un lenguaje extensamente conocido por la comunidad, y gracias a la extensa documentación y ayuda de la comunidad de desarrolladores,  permitió que se produjeran más programas que explotan el paralelismo que ofrecen las GPU's.

Con estos avances en la computación paralela, se abre el camino a la creación de arquitecturas y estructuras de procesamiento paralelo distribuidas, los clúster. Estas estructuras aúnan muchos recursos hardware en un entorno paralelo distribuido que permite explotar al máximo las capacidades computacionales de los procesadores y así lograr que algoritmos que antes se consideraban como intratables a nivel computacional puedan ser computados en un tiempo asumible gracias a la división de trabajo.

\section{Motivación y Contexto}
\label{sec:motivacion}

Desde hace unas décadas, los sistemas de altas prestaciones, HPC y clúster están presentes en nuestra sociedad. Gracias al avance de este tipo de infraestructuras, ha sido posible resolver problemas de gran coste computacional como, por ejemplo, la predicción de la meteorología con una precisión suficiente para que sus resultados sean relevantes, así como su uso para la simulación de fármacos, procesos químicos, predicción de catástrofes, etc... Por estos motivos, el crecimiento en tamaño, complejidad y número de estos superordenadores se hace patente. Cada vez, más países disponen de uno o varios de estos clúster para poder satisfacer la demanda de procesamiento, convirtiéndose en un recurso indispensable para que la comunidad científica pueda seguir avanzando en diversos campos. Aunque estos equipos proliferan, no suelen estar al alcance de estudiantes o para pequeños proyectos. Esto es debido a que el coste de estos sistemas es muy alto, ya que, para su construcción, se necesita una gran inversión económica destinada al equipo informático y a los sistemas auxiliares. Además, hay que sumar el coste del mantenimiento de toda su infraestructura, así como el gasto por consumo eléctrico que suele ser elevado. Por todo ello este tipo de instalaciones siguen siendo escasas y de difícil acceso.

En el caso de la Universidad de Alicante, el Instituto Universitario de Investigación Informática IUII, dispone de un clúster de cálculo llamado Euler orientado a proyectos de investigación en todas las disciplinas que necesiten potencia de cálculo para sus proyectos. Este servicio queda fuera del alcance de los alumnos, ya que su uso se limita a proyectos de investigación. Sin embargo, en la Escuela Politécnica Superior de Alicante EPSA, se dispone de un laboratorio de altas prestaciones equipado con tarjetas gráficas GTX 480, figura \ref{fig:GTX_480}, cedidas por NVIDIA. Gracias a la relación de NVIDIA con la UA, siendo esta GPU Educaction Center y GPU Research Center, avalados por NVIDIA.
\begin{figure}[H]
    \centering
    \includegraphics[width=8cm]{images/gtx480.png}
    \caption{GTX 480 Cedidas por NVIDIA}
    \label{fig:GTX_480}
\end{figure}
Gracias a la donación recibida, este laboratorio dispone de una gran cantidad de recursos computacionales, y puede explotarse como un recurso en bruto de gran interés. Por ello se planteó la creación de un sistema para aprovechar el laboratorio en las horas que no tuviera un uso docente. Con la guía de los profesores José García (director del GPU Research Center) y Juan Antonio Gil (director de tecnologías de la EPS), se creó el proyecto de \textit{Cluster as a service}, que desarrollé durante el curso académico 2016-2017 en prácticas en empresa I y II junto a mi compañero Andrés Carpena. En el proyecto se implementó un sistema para el aprovechamiento de este recurso, para lo que se diseñó y se creó un clúster que fuera compatible con el uso normal del aula.

La motivación personal para la realización de este proyecto viene dada por los años en los que he trabajado en montaje de redes de ordenadores, así como el gran apoyo constante de mi padre (Juan Rodríguez López- Ingeniero en Informática) que me facilitó desde bien pequeño el introducirme en el mundo de los ordenadores. Por todo ello, este proyecto ha servido como motivación personal para mejorar mis capacidades como futuro ingeniero y además supone un orgullo que mi proyecto no quede solo en la teoría, sino que, además, he podido ver como se ha materializado y se ofrece actualmente como recurso en la Escuela Politécnica Superior de la Universidad de Alicante (aunque todavía en pruebas por razones ajenas a mi trabajo).

El valor de este Proyecto de final de Grado no se limita a superar una asignatura obligatoria del grado de Informática, sino que se ha materializado en un recurso de utilidad a la comunidad, pudiendo conseguir que sea utilizado por los alumnos de la UA que necesiten recursos de altas prestaciones o recursos remotos para las aplicaciones. Además, gracias a este proyecto, he logrado abrir la puerta a los supercomputadores a un público más amplio.
